## Table of contents
* [General info](#general_info)
* [Technologies](#technologies)
* [Used packages](#used_packages)
* [Presentation](#presentation)
* [Setup](#setup)

## General info
This project was bootstrapped with Create React App.\
This project presents simple 1 page responsive webside.

## Technologies
* React version : 17.0.1
* Sass version 1.32.8

## Used packages
* Font Awesome -- https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers
* Hamburger React -- https://www.npmjs.com/package/hamburger-react
* React Scroll -- https://www.npmjs.com/package/react-scroll

## Presentation
* Desktop version:\
![Algorithm schema](./presentation/desktop1.png)\
![Algorithm schema](./presentation/desktop2.png)\
![Algorithm schema](./presentation/desktop3.png)\
![Algorithm schema](./presentation/desktop4.png)\
![Algorithm schema](./presentation/desktop5.png)\
![Algorithm schema](./presentation/desktop6.png)
* Tablet version:\
![Algorithm schema](./presentation/tablet1.png)\
![Algorithm schema](./presentation/tablet2.png)\
![Algorithm schema](./presentation/tablet3.png)
* Phone version:\
![Algorithm schema](./presentation/phone1.png)\
![Algorithm schema](./presentation/phone2.png)\
![Algorithm schema](./presentation/phone3.png)\
![Algorithm schema](./presentation/phone4.png)\
![Algorithm schema](./presentation/phone5.png)\
![Algorithm schema](./presentation/phone6.png)\
![Algorithm schema](./presentation/phone7.png)\
![Algorithm schema](./presentation/phone8.png)\

## Setup 

$ git clone https://gitlab.com/nazarradev/simple-one-page-website.git \
$ npm install \
$ npm start



