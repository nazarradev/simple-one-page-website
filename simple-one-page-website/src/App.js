import React from 'react';
import "./components/FontawsomeIcons"
import Header from "./components/Header"
import DescriptionSection from "./components/DescriptionSection";
import BenefitSection from "./components/BenefitSection";
import PricingSection from "./components/PricingSection";
import ContactUsSection from "./components/ContactUsSection";
import Footer from "./components/Footer";



function App() {
  return (
    <div className="App">
      <Header/>
      <DescriptionSection/>
      <BenefitSection/>
      <PricingSection/>
      <ContactUsSection/>
      <Footer/>
    </div>
  );
}

export default App;
