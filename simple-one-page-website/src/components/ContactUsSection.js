import React, {useState} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const ContactUsSection = () =>{
    const [name,setName] = useState("")
    const [email,setEmail] = useState("")

    let DecorationColor="#08A6E4";

    return(
        <>
            <section className={"Contact_Us"} name={'Contact'}>
                <div className={'contact_us_description_container'}>
                <div className={"contact_us_header"}>
                    <h3>Any questions?</h3>
                    <p>Leave your email address or call us!</p>
                </div>
                <div className={'contact_us_description'}>
                    <p>
                        <FontAwesomeIcon icon={"envelope"} color={DecorationColor} size={"lg"}/>
                        {" " + "info@bestshop.xyz"}
                    </p>
                    <p>
                        <FontAwesomeIcon icon={"phone"} color={DecorationColor} size={"lg"}/>
                        {"123456789"}
                    </p>
                </div>
                </div>
                <form>
                    <label>NAME</label>
                        <input type={'text'} value={name} onChange={e => setName(e.target.value)}/>
                    <label>EMAIL </label>
                        <input type={'email'} value={email} onChange={e => setEmail(e.target.value)}/>
                    <div>
                        <input type={'checkbox'} name={'regulations'} />
                        <label className={'checkbox_label'}>
                            I hereby give consent for my personal data included in my application to be processed for the purposes of the recruitment process under the European Parliament’s and Council of the European Union Regulation on the Protection of Natural Persons as of 27 April 2016, with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC (Data Protection Directive)
                        </label>
                    </div>
                    <input type="submit" value="Send" className={'form_button'} />
                </form>
            </section>
        </>
    )
}
export default ContactUsSection

