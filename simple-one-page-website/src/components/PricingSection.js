import React from 'react';
import PricingComponent from "./PricingComponent";
import '../scss/setting/_colors.scss'

const PricingSection = () =>{
    let greenColor = '#55DFB4';
    let redColor = '#FB3B64';
    return(
        <>
            <section className={'pricing_section'} name={'Prices'}>
                <h3>Pricing</h3>
                <div className={'price_container'}>
                    <PricingComponent classname={'basic'}
                                      title={'Basic'}
                                      price={'$0'}
                                      spaceColor={greenColor}
                                      spaceDescription={'100 MB HDD'}
                                      subdomainColor={greenColor}
                                      subdomainDescription={'1 Subdomain'}
                                      emailColor={greenColor}
                                      emailDescription={'2 E-mails'}
                                      licenseColor={redColor}
                                      licenseDescription={'Two years license'}
                                      supportColor={redColor}
                                      supportDescription={'Full support'}
                                      buttonClassName={'basic_component_button'}
                                      />
                    <PricingComponent classname={'professional'}
                                      title={'Professional'}
                                      price={'$25'}
                                      spaceColor={greenColor}
                                      spaceDescription={'500 MB HDD'}
                                      subdomainColor={greenColor}
                                      subdomainDescription={'2 Subdomains'}
                                      emailColor={greenColor}
                                      emailDescription={'5 E-mails'}
                                      licenseColor={greenColor}
                                      licenseDescription={'One year license'}
                                      supportColor={redColor}
                                      supportDescription={'Full support'}
                                      buttonClassName={'professional_component_button'}
                    />
                    <PricingComponent classname={'premium'}
                                      title={'Premium'}
                                      price={'$60'}
                                      spaceColor={greenColor}
                                      spaceDescription={'2 GB HDD'}
                                      subdomainColor={greenColor}
                                      subdomainDescription={'5 Subdomains'}
                                      emailColor={greenColor}
                                      emailDescription={'20 E-mails'}
                                      licenseColor={greenColor}
                                      licenseDescription={'Two years license'}
                                      supportColor={greenColor}
                                      supportDescription={'Full support'}
                                      buttonClassName={'premium_component_button'}
                    />
                </div>
            </section>

        </>
    )
}

export default PricingSection;