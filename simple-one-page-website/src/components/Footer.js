import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Footer = () =>{
    return(
        <>
            <footer>
                <section className={'footer_description'}>
                    <a href={'./'} >BestShop</a>
                    <p>© 2019 BestShop LTD, All Rights Reserved</p>
                </section>
                <section className={"footer_media"}>

                    <a href={'https://twitter.com/' }>
                        <FontAwesomeIcon icon={['fab', 'twitter-square']} color={'#1DA1F2'} size={"2x"}/>
                    </a>
                    <a href={'https://facebook.com/'}>
                        <FontAwesomeIcon icon={['fab', 'facebook-square']} color={'#4267B2'} size={"2x"}/>
                    </a>
                </section>
            </footer>
        </>
    )
}

export default Footer;