import React from "react";
import PricingDescriptionComponent from "./PricingDescriptionComponent";


const PricingComponent = (props) =>{
    return(
        <>
            <div className={`pricing_component ${props.classname}`}>
                <div className={'pricing_header'}>
                    <h4 className={'pricing_title'}>{props.title}</h4>
                    <p className={"price"}>{props.price}</p>
                    <p className={'offer'}>Limited offer</p>
                </div>
                <div className={'pricing_description'}>
                    <PricingDescriptionComponent priceClassName={'space'} color={props.spaceColor} description={props.spaceDescription} />
                    <PricingDescriptionComponent priceClassName={'subdomain'} color={props.subdomainColor} description={props.subdomainDescription} />
                    <PricingDescriptionComponent priceClassName={'email'} color={props.emailColor} description={props.emailDescription} />
                    <PricingDescriptionComponent priceClassName={'license'} color={props.licenseColor} description={props.licenseDescription} />
                    <PricingDescriptionComponent priceClassName={'support'} color={props.supportColor} description={props.supportDescription} />
                </div>
                <a href={"./"} className={`pricingButton ${props.buttonClassName}`} >Begin</a>
            </div>
        </>
    )

}

export default PricingComponent;