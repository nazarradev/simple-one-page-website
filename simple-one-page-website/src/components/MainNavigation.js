import React from "react";
import ScrollLink from "./ScrollLink";

const MainNavigation = (props) =>{

    return(
        <>
            <nav className={`navigation ${props.NaviClassName}` }>
                <ul>
                    <ScrollLink linkTarget={"Description"}  LinkName={"WHY US"} hamburgerOpen={props.hamburgerOpen} />
                    <ScrollLink linkTarget={"Benefits"}  LinkName={"BENEFITS"} hamburgerOpen={props.hamburgerOpen}  />
                    <ScrollLink linkTarget={"Prices"}  LinkName={"PRICES"} hamburgerOpen={props.hamburgerOpen} />
                    <ScrollLink linkTarget={"Contact"}  LinkName={"CONTACT"} hamburgerOpen={props.hamburgerOpen}/>
                </ul>
            </nav>

        </>
    )
}

export default MainNavigation;