import { library } from '@fortawesome/fontawesome-svg-core'

import {faCheckCircle,faEnvelope,faPhone,} from '@fortawesome/free-solid-svg-icons'

import { fab,faTwitterSquare,faFacebookSquare } from '@fortawesome/free-brands-svg-icons'

library.add(faCheckCircle,faEnvelope,faPhone,fab,faTwitterSquare,faFacebookSquare)
