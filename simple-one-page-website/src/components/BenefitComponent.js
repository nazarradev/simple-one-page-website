import React from "react";

const BenefitComponent = (props) =>{
    return(
        <>
            <div className={`benefit_component ${props.classname}`}>
                <div className={"benefit_description_container"}>
                    <h4>{props.title}</h4>
                    <p>{props.description}</p>
                </div>
                <div className={"benefit_image_container"}>
                    <div>
                    <img src={props.image} alt={"Logo"} />
                    </div>
                </div>
            </div>
        </>
    )
};

export default BenefitComponent;