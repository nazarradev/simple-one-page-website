import React from "react";
import {Link} from 'react-scroll'

const ScrollLink=(props)=> {
    const handleDoneClick = () => {

        if (typeof props.hamburgerOpen === "function"){
            props.hamburgerOpen();
        }
    };
   return(
    <>
        <li>
            <div>
                <Link to={props.linkTarget}
                      smooth={true}
                      duration={500}
                >
                    <a onClick={handleDoneClick} href={"/"}> {props.LinkName}  </a>
                </Link>
            </div>
        </li>
    </>
)}

export default ScrollLink;
