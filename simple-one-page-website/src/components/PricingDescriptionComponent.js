import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"

const PricingDescriptionComponent = (props) =>{
    return(
        <>

                    <p className={props.priceClassName}>
                        <FontAwesomeIcon icon={"check-circle"} color={props.color} size={"xs"}/>
                        {" " + props.description}
                    </p>
        </>
    )

}

export default PricingDescriptionComponent;