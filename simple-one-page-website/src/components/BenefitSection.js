import React from "react";
import BenefitComponent from "./BenefitComponent";
import First_Benefit_Icon from "../assets/Macbook2.png";
import Second_Benefit_icon from '../assets/iPhone2.png';
import Third_Benefit_Icon from '../assets/Trumpet.png';

const BenefitSection = () =>{
    return(
        <>
            <section className={"Benefit_Section"} name={"Benefits"}>
                <BenefitComponent classname={'first_benefit'}
                                  title={"Be always first"}
                                  description={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."}
                                  image={First_Benefit_Icon}
                                  />
                <BenefitComponent classname={'second_benefit'}
                                  title={"Your shop is where you are!"}
                                  description={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."}
                                  image={Second_Benefit_icon}
                />
                <BenefitComponent classname={'third_benefit'}
                                  title={"Increase recognition your brand!"}
                                  description={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."}
                                  image={Third_Benefit_Icon}
                />
            </section>
        </>
    )
};

export default BenefitSection;